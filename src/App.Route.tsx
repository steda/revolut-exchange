import React from 'react';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom';
import { WaitingComponent } from './components';
import { APP_PATHS } from './constants';

export const AppRoute = () => {
    return (
        <HashRouter>
            <Switch>
                <Route exact
                    path={APP_PATHS.ACCOUNTS.path}
                    component={
                        WaitingComponent(
                            React.lazy(() =>
                                import('./pages/Accounts/AccountsPage').then((module) => ({
                                    default: module.AccountsPage
                                }))
                            )
                        )}
                />
                <Route exact
                    path={APP_PATHS.EXCHANGE.path}
                    component={
                        WaitingComponent(
                            React.lazy(() =>
                                import('./pages/Exchange/ExchangePage').then((module) => ({
                                    default: module.ExchangePage
                                }))
                            )
                        )}
                />
                <Route render={() => <Redirect to={APP_PATHS.ACCOUNTS.path} />} />
            </Switch>
        </HashRouter>
    );
}