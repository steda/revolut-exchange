import React, { FunctionComponent, useEffect, useState } from 'react';

import { useAccounts } from './hooks';
import { AppRoute } from './App.Route';
import { AccountsContext } from './contexts/Accounts.Context';
import { Account } from './models';
import './App.scss';

const App: FunctionComponent = () => {
  const { accounts } = useAccounts();
  const [selectedAccount, setSelectedAccount] = useState<Account>(accounts && accounts[0]);
  const [activeAccounts, setActiveAccounts] = useState<Account[]>(accounts);

  const onAccountSelected = (account: Account) => {
    setSelectedAccount(account);
  }

  const setAccounts = (accounts: Account[]) => {
    setActiveAccounts([...accounts]);
  }

  useEffect(() => {
    setSelectedAccount(accounts[0]);
    setActiveAccounts(accounts);
  }, [accounts]);

  return (
    <div className="App">
      <AccountsContext.Provider value={{
        availableAccounts: activeAccounts,
        selectedAccount: selectedAccount,
        onAccountSelected,
        setAccounts
      }}>
        <AppRoute />
      </AccountsContext.Provider>
    </div >
  );
}

export default App;
