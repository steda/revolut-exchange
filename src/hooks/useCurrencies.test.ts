import { cleanup, renderHook } from '@testing-library/react-hooks'
import { useCurrencies } from './useCurrencies';

afterEach(cleanup);

test("useCurrencies should fetch currencies", async () => {
    const { result, waitForNextUpdate } = renderHook(useCurrencies);

    expect(result.current.currencies).toEqual([]);
    expect(result.current.currenciesError).toEqual(undefined);
    expect(result.current.currenciesLoading).toEqual(true);

    await waitForNextUpdate();
    expect(result.current.currencies).toBeTruthy();
    expect(result.current.currencies).toHaveLength(3);
    expect(result.current.currenciesError).toBe(undefined);
    expect(result.current.currenciesLoading).toBe(false);
});