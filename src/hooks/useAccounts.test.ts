import { cleanup, renderHook } from '@testing-library/react-hooks'
import { useAccounts } from './useAccounts'

jest.mock('axios');

afterEach(cleanup);

test("useAccounts should fetch accounts", async () => {
    const { result, waitForNextUpdate } = renderHook(useAccounts);

    expect(result.current.accounts).toEqual([]);
    expect(result.current.accountsLoading).toEqual(true);
    expect(result.current.accountsError).toEqual(undefined);

    await waitForNextUpdate();
    expect(result.current.accounts).toHaveLength(3);
    expect(result.current.accountsLoading).toEqual(false);
    expect(result.current.accountsError).toEqual(undefined);
});