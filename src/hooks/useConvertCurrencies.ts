import { AvailableCurrencyType, ConversionRate } from "../models";
import { useState } from 'react';
import { useEffect } from 'react';
import { convertCurrencies } from "../services";

interface ConvertCurrenciesnHook {
    convertionRate: number;
    convertionError: any;
    convertionLoading: boolean;
}

export const useConvertCurrencies = (
    firstCurrency?: AvailableCurrencyType,
    secondCurrency?: AvailableCurrencyType,
    value: number = 1): ConvertCurrenciesnHook => {
    const [convertionRate, setConvertionRate] = useState<number>(1);
    const [convertionError, setConvertionError] = useState<any>();
    const [convertionLoading, setConvertionLoading] = useState<boolean>(false);


    useEffect(() => {
        if (!firstCurrency || !secondCurrency) {
            return;
        }

        console.log('firstCurrency',firstCurrency);
        console.log('secondCurrency', secondCurrency);

        setConvertionLoading(true);

        convertCurrencies(value, firstCurrency, secondCurrency)
            .then(({ rate }: ConversionRate) => setConvertionRate(rate))
            .catch(error => setConvertionError(error))
            .finally(() => setConvertionLoading(false));
    }, [firstCurrency, secondCurrency, value]);

    useEffect(() => {
        if (!firstCurrency || !secondCurrency) {
            return;
        }

        setConvertionLoading(true);

        const interval = setInterval(() =>
            convertCurrencies(value, firstCurrency, secondCurrency)
                .then(({ rate }: ConversionRate) => setConvertionRate(rate))
                .catch(error => setConvertionError(error))
                .finally(() => setConvertionLoading(false))
            , 10000);

        return () => clearInterval(interval);
    }, [firstCurrency, secondCurrency, value]);

    return {
        convertionRate,
        convertionError,
        convertionLoading
    }
}