import { useEffect, useState } from "react"
import { Account } from "../models";
import { getAvailableAccounts } from "../services"

interface AccountHook {
    accounts: Account[] | [];
    accountsError: any;
    accountsLoading: boolean;
}

export const useAccounts = (): AccountHook => {
    const [accounts, setAccounts] = useState<Account[] | []>([]);
    const [accountsError, setAccountsError] = useState<any>();
    const [accountsLoading, setAccountsLoading] = useState<boolean>(false);

    useEffect(() => {
        setAccountsLoading(true);

        getAvailableAccounts()
            .then((response: Account[]) => setAccounts(response))
            .catch(error => setAccountsError(error))
            .finally(() => setAccountsLoading(false));
    }, []);

    return {
        accounts,
        accountsError,
        accountsLoading
    }
}