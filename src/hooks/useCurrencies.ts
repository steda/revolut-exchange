import { useEffect, useState } from "react"
import { ICurrency } from '../models/Currency.model';
import { getAvailableCurrencies } from "../services";

interface CurrenciesHook {
    currencies: ICurrency[] | [];
    currenciesError: any;
    currenciesLoading: boolean;
}

export const useCurrencies = (): CurrenciesHook => {
    const [currencies, setCurrencies] = useState<ICurrency[] | []>([]);
    const [currenciesError, setCurrenciesError] = useState<any>();
    const [currenciesLoading, setCurrenciesLoading] = useState<boolean>(false);

    useEffect(() => {
        setCurrenciesLoading(true);

        getAvailableCurrencies()
            .then((response: ICurrency[]) => setCurrencies(response))
            .catch(error => setCurrenciesError(error))
            .finally(() => setCurrenciesLoading(false));
    }, []);

    return {
        currencies,
        currenciesError,
        currenciesLoading
    }
}