import { cleanup, renderHook } from '@testing-library/react-hooks'
import { useConvertCurrencies } from './useConvertCurrencies';

afterEach(cleanup);

test("useConvertCurrencies should have correct defaults", async () => {
    const { result, waitForNextUpdate } = renderHook(useConvertCurrencies);

    expect(result.current.convertionRate).toEqual(1);
    expect(result.current.convertionError).toEqual(undefined);
    expect(result.current.convertionLoading).toEqual(false);
});

test("useConvertCurrencies should fetch conversionRate", async () => {
    let { result, waitForNextUpdate } = renderHook(() => useConvertCurrencies("EUR", "USD"));

    expect(result.current.convertionRate).toEqual(1);
    expect(result.current.convertionError).toEqual(undefined);
    expect(result.current.convertionLoading).toEqual(true);

    await waitForNextUpdate();

    expect(result.current.convertionRate).not.toEqual(1);
    // This wouldn't be possible in a real world example so we 
    // are checking to be different then default value.
    expect(result.current.convertionRate).toEqual(1.22);
    expect(result.current.convertionError).toEqual(undefined);
    expect(result.current.convertionLoading).toEqual(false);
});