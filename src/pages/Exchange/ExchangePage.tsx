import React, { FunctionComponent, useEffect } from 'react';
import { Exchange } from '../../components';
import { APP_PATHS } from '../../constants';

export const ExchangePage: FunctionComponent<{}> = () => {
    
    useEffect(() => {
        document.title = APP_PATHS.EXCHANGE.title
    }, []);

    return (
        <Exchange />
    )
}