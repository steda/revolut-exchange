import { Button } from 'antd';
import React, { FunctionComponent, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AccountSelect, RevolutCard } from '../../components';
import { APP_PATHS } from '../../constants/AppPaths.const';
import { AccountsContext } from '../../contexts/Accounts.Context';
import { AvailableCurrencyType } from '../../models';
import { Account } from '../../models/Account.model';
import './AccountsPage.scss';


export const AccountsPage: FunctionComponent<{}> = () => {
    const { availableAccounts, selectedAccount, onAccountSelected } = useContext(AccountsContext);

    const onAccountChange = (value: AvailableCurrencyType): void => {
        const account: Account | undefined = availableAccounts.find((account) => account.currency === value);
        if (!account) return;

        onAccountSelected(account);
    };

    useEffect(() => {
        document.title = APP_PATHS.ACCOUNTS.title
    }, []);

    return (
        <div className="AccountsPage">
            <h1>Accounts</h1>
            <RevolutCard>
                <div className="AccountsPage-header">
                    Current: <span>{selectedAccount?.balance} {selectedAccount?.currency}</span>
                    <span>
                        <Link to={{
                            pathname: APP_PATHS.EXCHANGE.path,
                            state: {
                                selectedCurrency: selectedAccount?.currency
                            }
                        }}>
                            <Button type="link" size="large"> Exchange</Button>
                        </Link>
                    </span>
                </div>
                <div className="AccountsPage-body">
                    <div className="AccountsPage-account-select-label">Select another account</div>
                    <AccountSelect
                        options={availableAccounts}
                        onChange={onAccountChange}
                        placeholder={'Search for another account...'}
                    />
                </div>
            </RevolutCard>
        </div >
    )
}