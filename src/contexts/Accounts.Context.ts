import React from "react";
import { Account } from '../models';

export const AccountsContext = React.createContext({
    availableAccounts: [] as Account[],
    selectedAccount: {} as Account,
    onAccountSelected: (account: Account) => { },
    setAccounts: (accounts: Account[]) => { }
});