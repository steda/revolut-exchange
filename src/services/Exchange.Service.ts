
import { AvailableCurrencyType, ConversionRate, ExchangeLatestRatesResponse, ExchangeRates } from '../models';
import { API_INSTANCE, parseAxiosResponse } from './Api.Service.Utils';


// {
//     "error": true,
//     "status": 403,
//     "message": "not_allowed",
//     "description": "Changing the API `base` currency is available for Developer,
//       Enterprise and Unlimited plan clients. Please upgrade, or contact support@openexchangerates.org 
//      with any questions."
//   }
export const getLatestRates = (selectedCurrency: AvailableCurrencyType): Promise<ExchangeRates> => {
    return API_INSTANCE.get('latest.json', { params: { base: selectedCurrency } })
        .then(parseAxiosResponse)
        .then(_mapLatestRatesResponse);
}


// description: "Single-currency Conversion API not available with this App ID - please contact support@openexchangerates.org to upgrade your account."
// error: true
// message: "not_allowed"
// status: 403
export const convertCurrencies = (
    value: number,
    fromCurrency: AvailableCurrencyType,
    toCurrency: AvailableCurrencyType
): Promise<ConversionRate> => {
    return API_INSTANCE.get(`convert/${value}/${fromCurrency}/${toCurrency}`)
        .then(parseAxiosResponse)
        .catch(_mockedData.bind(this, fromCurrency, toCurrency));
}


//////////////////////////
///// Private Helpers ////
//////////////////////////
const _mockedData = (
    fromCurrency: AvailableCurrencyType,
    toCurrency: AvailableCurrencyType
) => {
    const MOCK_OBJECT = {
        USD: { EUR: 0.82, GBP: 0.7, USD: 1 },
        EUR: { GBP: 0.86, USD: 1.22, EUR: 1 },
        GBP: { USD: 1.42, EUR: 1.16, GBP: 1 }
    }

    return new Promise((resolve) => {
        resolve({ rate: MOCK_OBJECT[fromCurrency][toCurrency] })
    });
}

const _mapLatestRatesResponse = ({ rates, base }: ExchangeLatestRatesResponse) => {
    return {
        rates,
        base
    }
}


