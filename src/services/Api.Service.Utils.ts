import axios, { AxiosResponse } from 'axios';

const ON_EXCHANGE_APP_ID = '700bfb29afd64286820a5547d76edfcd';

export const API_INSTANCE = axios.create({
    baseURL: `https://openexchangerates.org/api/`,
    params: {
        app_id: ON_EXCHANGE_APP_ID
    }
});

export const parseAxiosResponse = ({ data }: AxiosResponse): any => data;