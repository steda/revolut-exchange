import { AVAILABLE_CURRENCIES } from '../data/AvailableCurrencies.const';
import { ICurrency } from '../models/Currency.model';

export const getAvailableCurrencies = (): Promise<ICurrency[]> => {
    return new Promise((resolve) => {
        resolve(Object.values(AVAILABLE_CURRENCIES) as ICurrency[]);
    });
}