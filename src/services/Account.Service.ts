import { AVAILABLE_ACCOUNTS } from "../data/AvailableAccounts.const";
import { Account } from '../models';

export const getAvailableAccounts = (): Promise<Account[]> => {
    return new Promise((resolve) => {
        resolve(AVAILABLE_ACCOUNTS);
    })
}