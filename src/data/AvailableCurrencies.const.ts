export const AVAILABLE_CURRENCIES = {
    USD: {
        label: 'United States Dollar',
        code: 'USD',
        symbol: '$'
    },
    EUR: {
        label: 'Euro',
        code: 'EUR',
        symbol: '€'
    },
    GBP: {
        label: 'British Pound',
        code: 'GBP',
        symbol: '£'
    }
}