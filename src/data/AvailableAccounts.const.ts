import { AVAILABLE_CURRENCIES } from './AvailableCurrencies.const';
import { Account } from '../models/Account.model';
import { AvailableCurrencyType } from '../models';

export const AVAILABLE_ACCOUNTS: Account[] = [
    {
        currency: AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType,
        balance: 77
    },
    {
        currency: AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType,
        balance: 18
    },
    {
        currency: AVAILABLE_CURRENCIES.GBP.code as AvailableCurrencyType,
        balance: 200
    }
];