export const APP_PATHS = {
    ACCOUNTS: {
        title: 'Accounts',
        path: '/accounts'
    },
    EXCHANGE: {
        title: 'Exchange',
        path: '/exchange'
    }
}