import { EXCHANGE_ACTION_TYPE_ENUM } from "../models";

export const EXCHANGE_ACTION_TYPE = {
    [EXCHANGE_ACTION_TYPE_ENUM.BUY]: {
        value: EXCHANGE_ACTION_TYPE_ENUM.BUY,
        label: 'Buy'
    },
    [EXCHANGE_ACTION_TYPE_ENUM.SELL]: {
        value: EXCHANGE_ACTION_TYPE_ENUM.SELL,
        label: 'Sell'
    }
}