import { AvailableCurrencyType } from "./AvailableCurrency.model";

export interface Account {
    currency: AvailableCurrencyType;
    balance: number;
}