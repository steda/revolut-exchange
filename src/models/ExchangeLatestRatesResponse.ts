import { AvailableCurrencyType } from "./AvailableCurrency.model";

export interface ExchangeLatestRatesResponse {
    disclaimer: string,
    license: string,
    timestamp: Date,
    base: AvailableCurrencyType,
    rates: Rates;
}

export interface Rates {
    [key: string]: number;
}

export interface ExchangeRates {
    base: AvailableCurrencyType;
    rates: Rates;
}

export interface ConversionRate {
    rate: number;
}