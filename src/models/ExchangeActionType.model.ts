export enum EXCHANGE_ACTION_TYPE_ENUM {
    SELL = 'SELL',
    BUY = 'BUY'
}