import { AvailableCurrencyType } from "./AvailableCurrency.model";

export interface ICurrency {
    label: string;
    code: AvailableCurrencyType;
    symbol: string;
}