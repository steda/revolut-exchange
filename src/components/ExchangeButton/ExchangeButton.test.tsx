import { screen, render, RenderOptions, fireEvent } from '@testing-library/react';
import React from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import { AVAILABLE_CURRENCIES } from '../../data/AvailableCurrencies.const';
import { ExchangeButton } from './ExchangeButton';

test("<ExchangeButton /> should matches snapshot", () => {
    const component = render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />
    );
    expect(component.container).toMatchSnapshot();
});

test('<ExchangeButton /> should renders without crashing', () => {
    render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeButton /> should render correct values when `actionType` is SELL', async () => {
    const assertText = 'SELL EUR for USD';

    render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);

    const button = await screen.findByRole('button');

    expect(button.textContent).toEqual(assertText);
});

test('<ExchangeButton /> should render correct values when `actionType` is BUY', async () => {
    const assertText = 'BUY EUR with USD';

    render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.BUY}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);

    const button = await screen.findByRole('button');

    expect(button.textContent).toEqual(assertText);
});

test('<ExchangeButton /> should render passed currencies', async () => {
    render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.BUY}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);

    const button = await screen.findByRole('button');
    const textList = button.textContent?.split('');

    expect(textList?.filter((currency) => currency === AVAILABLE_CURRENCIES.EUR.code)).toBeTruthy();
    expect(textList?.filter((currency) => currency === AVAILABLE_CURRENCIES.USD.code)).toBeTruthy();
});

test('<ExchangeButton /> should fire onClick event', async () => {
    let counter = 0;
    render(
        <ExchangeButton
            actionType={EXCHANGE_ACTION_TYPE_ENUM.BUY}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
            onClick={() => counter++}
        />);

    const button = await screen.findByRole('button');
    fireEvent.click(button);

    expect(counter).toEqual(1);
});