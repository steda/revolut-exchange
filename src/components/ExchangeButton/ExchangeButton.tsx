import { Button } from 'antd';
import React, { FunctionComponent, PropsWithChildren } from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';

interface Props {
    actionType: EXCHANGE_ACTION_TYPE_ENUM;
    fromCurrency: AvailableCurrencyType;
    toCurrency: AvailableCurrencyType;
    onClick?: () => void;
    buttonType?: 'button' | 'submit';
}

export const ExchangeButton: FunctionComponent<PropsWithChildren<Props>> = ({
    actionType,
    fromCurrency,
    toCurrency,
    buttonType = 'button',
    onClick
}) => {
    return (
        <Button
            style={{ margin: '40px 0' }}
            type="primary"
            shape="round"
            size="large"
            onClick={onClick}
            htmlType={buttonType}
            block>
            {actionType}
            {' '}
            {fromCurrency}
            {actionType === EXCHANGE_ACTION_TYPE_ENUM.SELL ? ' for ' : ' with '}
            {toCurrency}
        </Button>
    );
}