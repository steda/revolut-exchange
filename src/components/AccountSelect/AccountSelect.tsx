import React, { FunctionComponent } from 'react';
import { Select } from 'antd';
import { AvailableCurrencyType, Account } from '../../models';
import './AccountSelect.scss';

const { Option } = Select;

interface Props {
    options: Account[];
    placeholder?: string;
    onChange: (value: AvailableCurrencyType) => void;
}

export const AccountSelect: FunctionComponent<Props> = ({
    options = [],
    placeholder = 'Search...',
    onChange
}) => {
    // TODO: To fix select bug.
    const filterOptions = (input: string, option: any): boolean => {
        return option?.value.toLowerCase().indexOf(input.toLowerCase()) >= 0;
    }

    return (
        <Select
            className="AccountSelect"
            onChange={onChange}
            placeholder={placeholder}
            filterOption={filterOptions}>
            {options?.map((option: Account) => (
                <Option
                    key={option?.currency}
                    value={option.currency}>
                    {option.balance} {option.currency}
                </Option>)
            )}
        </Select>
    );
}