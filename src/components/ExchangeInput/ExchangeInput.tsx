import { Input, Select } from 'antd';
import React, { FunctionComponent } from 'react';
import { AvailableCurrencyType, ICurrency } from '../../models';
import './ExchangeInput.scss';
import { useState, useEffect } from 'react';
import { ExchangeInputBalance } from '../ExchangeInputBalance/ExchangeInputBalance';

const { Option } = Select;

interface Props {
    availableCurrencies: ICurrency[];
    defaultCurrency: AvailableCurrencyType;
    accountAmount: number;
    amount?: number;
    onCurrencyChange: (currency: AvailableCurrencyType) => void;
    onAmountChange?: (amount: number) => void;
}

export const ExchangeInput: FunctionComponent<Props> = ({
    availableCurrencies = [],
    defaultCurrency,
    amount = 0,
    accountAmount,
    onAmountChange,
    onCurrencyChange
}) => {
    const [amountValue, setAmountValue] = useState<number>(amount);

    const onAmountValueChange = (event: any) => {
        const newAmount: string = parseFloat(event?.target?.value).toFixed(2);
        setAmountValue(+newAmount);
        onAmountChange && onAmountChange(+newAmount);
    }

    useEffect(() => {
        setAmountValue(amount);
    }, [amount]);

    return (
        <>
            <Input.Group
                compact
                className="ExchangeInput">
                {availableCurrencies.length ?
                    <Select
                        defaultValue={defaultCurrency || availableCurrencies[0]?.code}
                        className="ExchangeInput-currency"
                        onChange={(currency) => onCurrencyChange(currency)}>
                        {
                            availableCurrencies.map(currency =>
                                <Option
                                    key={currency.code}
                                    value={currency.code}>
                                    {currency.code}
                                </Option>
                            )
                        }
                    </Select> :
                    null
                }
                <Input
                    className="ExchangeInput-amount"
                    type="number"
                    step="0.01"
                    onChange={onAmountValueChange}
                    value={amountValue}
                    placeholder={'0,00 ' + defaultCurrency || availableCurrencies[0]?.code} />
            </Input.Group>
            <ExchangeInputBalance
                amount={accountAmount}
                currency={defaultCurrency} />
        </>
    )
}