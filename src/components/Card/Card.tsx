import React, { FunctionComponent, PropsWithChildren } from 'react';
import './Card.scss';

export const RevolutCard: FunctionComponent<PropsWithChildren<{}>> = ({ children }) => {
    return (
        <div className="Card">
            {children}
        </div>
    )
}