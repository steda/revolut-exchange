import { screen, render, RenderOptions } from '@testing-library/react';
import React from 'react';
import { RevolutCard } from './Card';

test("<RevolutCard /> matches snapshot", () => {
    const component = render(
        <RevolutCard />
    );
    expect(component.container).toMatchSnapshot();
});

test('<RevolutCard /> renders without crashing', () => {
    render(
        <RevolutCard />,
        document.createElement('div') as RenderOptions
    );
});

test('component renders children', () => {
    const TEST_ID = 'CardChild';
    const component = render(
        <RevolutCard>
            <h1 data-testid={TEST_ID}>test</h1>
        </RevolutCard>
    );

    const content = screen.getAllByTestId(TEST_ID)[0];

    expect(component.container).toContainElement(content);
});