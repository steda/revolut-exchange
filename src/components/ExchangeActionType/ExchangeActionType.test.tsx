import { screen, render, RenderOptions, fireEvent } from '@testing-library/react';
import React from 'react';
import { EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import { ExchangeActionType } from './ExchangeActionType';

test("<ExchangeActionType /> should matches snapshot", () => {
    const component = render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.SELL}
        />
    );
    expect(component.container).toMatchSnapshot();
});

test('<ExchangeActionType /> should renders without crashing', () => {
    render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.SELL}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeActionType /> should render correct values when `actionType` is SELL', async () => {
    render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.SELL}
        />);

    const button = await screen.findByLabelText('arrow-down');

    expect(button).toBeTruthy();
    expect(button).toHaveClass('anticon-arrow-down');
});

test('<ExchangeActionType /> should render correct values when `actionType` is BUY', async () => {
    render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.BUY}
        />);

    const button = await screen.findByLabelText('arrow-up');

    expect(button).toBeTruthy();
    expect(button).toHaveClass('anticon-arrow-up');
});

test('<ExchangeActionType /> should have default action SELL', async () => {
    render(
        <ExchangeActionType />);

    const button = await screen.findByLabelText('arrow-down');

    expect(button).toBeTruthy();
    expect(button).toHaveClass('anticon-arrow-down');
});

test('<ExchangeActionType /> should toggle arrows on click', async () => {
    render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.SELL}
        />);

    let button = await screen.findByLabelText('arrow-down');

    expect(button).toBeTruthy();
    expect(button).toHaveClass('anticon-arrow-down');

    fireEvent.click(button);

    button = await screen.findByLabelText('arrow-up');

    expect(button).toBeTruthy();
    expect(button).toHaveClass('anticon-arrow-up');
});

test('<ExchangeActionType /> should fire onClick event', async () => {
    let action;
    render(
        <ExchangeActionType
            action={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            onActionChange={(actionType: EXCHANGE_ACTION_TYPE_ENUM) => action = actionType}
        />);

    const button = await screen.findByRole('button');
    fireEvent.click(button);

    expect(action).toEqual(EXCHANGE_ACTION_TYPE_ENUM.BUY);
});