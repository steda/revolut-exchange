import React, { useState, FunctionComponent } from 'react';
import { Button, Tooltip } from 'antd';
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';

import { EXCHANGE_ACTION_TYPE_ENUM } from '../../models';

interface Props {
    action: EXCHANGE_ACTION_TYPE_ENUM;
    onActionChange: (actionType: EXCHANGE_ACTION_TYPE_ENUM) => void
}

export const ExchangeActionType: FunctionComponent<Partial<Props>> = ({ action, onActionChange }) => {
    const [actionType, setActionType] =
        useState<EXCHANGE_ACTION_TYPE_ENUM>(action || EXCHANGE_ACTION_TYPE_ENUM.SELL);

    const buttonAction = actionType === EXCHANGE_ACTION_TYPE_ENUM.SELL ?
        <ArrowDownOutlined /> :
        <ArrowUpOutlined />;

    const onExchangeActionClicked = () => {
        const newActionType = actionType === EXCHANGE_ACTION_TYPE_ENUM.BUY ? EXCHANGE_ACTION_TYPE_ENUM.SELL :
            EXCHANGE_ACTION_TYPE_ENUM.BUY;

        setActionType(newActionType);
        onActionChange && onActionChange(newActionType);
    }

    return (
        <Tooltip title="Swap">
            <Button
                type="default"
                shape="circle"
                icon={buttonAction}
                onClick={onExchangeActionClicked} />
        </Tooltip>
    )
}