import React, { FunctionComponent, useCallback, useContext, useEffect, useState } from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM, ICurrency } from '../../models';
import { RevolutCard } from '../Card/Card';
import { useCurrencies } from '../../hooks/useCurrencies';
import { AccountsContext } from '../../contexts';
import { useConvertCurrencies } from '../../hooks';
import { notification, Spin } from 'antd';
import { Account } from '../../models/Account.model';
import { useHistory } from 'react-router-dom';
import { APP_PATHS } from '../../constants/AppPaths.const';
import { ExchangeHeader } from '../ExchangeHeader/ExchangeHeader';
import { ExchangeForm } from '../ExchangeForm/ExchangeForm';
import './Exchange.scss';

export const Exchange: FunctionComponent<{}> = () => {
    const { currencies } = useCurrencies();
    const { selectedAccount, availableAccounts, setAccounts } = useContext(AccountsContext);
    const history = useHistory();
    const [actionType, setActionType] =
        useState<EXCHANGE_ACTION_TYPE_ENUM>(EXCHANGE_ACTION_TYPE_ENUM.SELL);
    const [firstCurrency, setFirstCurrency] = useState<AvailableCurrencyType>(selectedAccount?.currency);
    const [secondCurrency, setSecondCurrency] = useState<AvailableCurrencyType>();
    const [isExchangeLoading, setIsExchangeLoading] = useState<boolean>(false);
    const { convertionRate } = useConvertCurrencies(firstCurrency, secondCurrency);

    const computeSecondCurrency = useCallback((currencies: ICurrency[]) => {
        const newCurrencyList: ICurrency[] = currencies.filter((currency: ICurrency) => currency.code !== firstCurrency);
        setSecondCurrency(newCurrencyList[0]?.code);
    }, [firstCurrency]);

    const onExchangeClick = (values: any): Promise<void> => {
        setIsExchangeLoading(true);

        const newAccounts: Account[] = availableAccounts.map((account: Account) => {
            if (account.currency === firstCurrency) {
                account.balance =
                    actionType === EXCHANGE_ACTION_TYPE_ENUM.SELL ?
                        account.balance - values.fromAmount : account.balance + values.fromAmount;
            }

            if (account.currency === secondCurrency) {
                account.balance = actionType === EXCHANGE_ACTION_TYPE_ENUM.SELL ?
                    account.balance + values.toAmount : account.balance - values.toAmount;
            }

            return account;
        });

        return new Promise((resolve) => resolve(newAccounts))
            .then(() => setAccounts(newAccounts))
            .then(() => notification.open({
                message: 'Success',
                description: 'We have successfully exchanged your amount!',
                type: 'success'
            }))
            .then(() => history.push(APP_PATHS.ACCOUNTS.path))
            .finally(() => setIsExchangeLoading(false));
    }

    useEffect(() => {
        computeSecondCurrency(currencies);
    }, [computeSecondCurrency, currencies]);

    return (
        <RevolutCard>
            {convertionRate && secondCurrency &&
                <ExchangeHeader
                    actionType={actionType}
                    convertionRate={convertionRate}
                    firstCurrency={firstCurrency}
                    secondCurrency={secondCurrency}
                />
            }
            <div className="Exchange-body">
                <Spin spinning={isExchangeLoading}>
                    {secondCurrency && convertionRate &&
                        <ExchangeForm
                            onSubmit={onExchangeClick}
                            onFirstCurrencyChange={setFirstCurrency}
                            onSecondCurrencyChange={setSecondCurrency}
                            onActionTypeChange={setActionType}
                            firstCurrency={firstCurrency}
                            secondCurrency={secondCurrency}
                            convertionRate={convertionRate}
                            availableAccounts={availableAccounts}
                            actionType={actionType}
                            currencies={currencies}
                        />
                    }
                </Spin>
            </div>
        </RevolutCard>
    );
}