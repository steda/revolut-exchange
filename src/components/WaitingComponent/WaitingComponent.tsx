import React from 'react';
import { Spin } from 'antd';

export function WaitingComponent(Component: React.LazyExoticComponent<any>) {
    return (props: any) => (
        <React.Suspense
            fallback={
                <Spin
                    spinning={true}
                    tip="Loading..." />
            }>
            <Component {...props} />
        </React.Suspense>
    );
}