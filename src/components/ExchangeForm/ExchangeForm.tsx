import { Form, FormInstance } from 'antd';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM, ICurrency } from '../../models';
import { ExchangeActionType } from '../ExchangeActionType/ExchangeActionType';
import { ExchangeButton } from '../ExchangeButton/ExchangeButton';
import { ExchangeInput } from '../ExchangeInput/ExchangeInput';
import { Account } from '../../models/Account.model';
import { Rule } from 'antd/lib/form';

interface Props {
    onSubmit: (values: any) => void | Promise<void>;
    onFirstCurrencyChange: (currency: AvailableCurrencyType) => void;
    onSecondCurrencyChange: (currency: AvailableCurrencyType) => void;
    onActionTypeChange: (actionType: EXCHANGE_ACTION_TYPE_ENUM) => void;
    convertionRate: number;
    firstCurrency: AvailableCurrencyType;
    secondCurrency: AvailableCurrencyType;
    availableAccounts: Account[];
    actionType: EXCHANGE_ACTION_TYPE_ENUM;
    currencies: ICurrency[] | [];
}

export const ExchangeForm: FunctionComponent<Props> = ({
    onSubmit,
    onFirstCurrencyChange,
    onSecondCurrencyChange,
    onActionTypeChange,
    convertionRate,
    firstCurrency,
    secondCurrency,
    availableAccounts,
    actionType,
    currencies
}) => {
    const formRef = React.createRef<FormInstance>();
    const [firstAmount, setFirstAmount] = useState<number>(0);
    const [secondAmount, setSecondAmount] = useState<number>(0);
    const [availableCurrencies, setAvailableCurrencies] = useState<ICurrency[] | []>([]);

    const getAccountAmount = (currency: AvailableCurrencyType) => {
        const account = availableAccounts.find((account) => account.currency === currency);
        return account ? account.balance : 0;
    }

    const onFromAmountChange = (value: number): void => {
        if (!value || !convertionRate) {
            return;
        }

        const computedValue = (value * convertionRate).toFixed(2);

        formRef?.current?.setFieldsValue({
            fromAmount: value,
            toAmount: +computedValue
        });

        setFirstAmount(value);
        setSecondAmount(+computedValue);
    }

    const onToAmountChange = (value: number): void => {
        if (!value || !convertionRate) {
            return;
        }

        const computedValue = (value / convertionRate).toFixed(2);

        formRef?.current?.setFieldsValue({
            fromAmount: +computedValue,
            toAmount: value
        });

        setFirstAmount(+computedValue);
        setSecondAmount(value);
    }

    const computeAvailableCurrencyList = useCallback((currencies: ICurrency[]) => {
        const EXCLUDED_ITEMS = [firstCurrency, secondCurrency];
        const filteredList: ICurrency[] = currencies.filter(currency => !EXCLUDED_ITEMS.includes(currency.code));
        setAvailableCurrencies(filteredList);
    }, [firstCurrency, secondCurrency]);

    const computeExchangeInputRules = (
        currency: AvailableCurrencyType,
        actionType: EXCHANGE_ACTION_TYPE_ENUM
    ): { fromExchangeRules: Rule[], toExchangeRules: Rule[] } => {
        const BASE_INPUT_RULES: Rule[] = [
            { required: true, message: 'A value is required' },
            { min: 1, type: 'number', message: 'A minimum value of 1 is required' }
        ];
        const MAX_INPUT_RULE: Rule = {
            max: getAccountAmount(currency),
            type: 'number',
            message: `You have ${getAccountAmount(currency)} ${currency} available in your ballance`
        };

        return actionType === EXCHANGE_ACTION_TYPE_ENUM.SELL ?
            {
                fromExchangeRules: [
                    ...BASE_INPUT_RULES,
                    MAX_INPUT_RULE
                ],
                toExchangeRules: [...BASE_INPUT_RULES]
            } :
            {
                fromExchangeRules: [...BASE_INPUT_RULES],
                toExchangeRules: [
                    ...BASE_INPUT_RULES,
                    MAX_INPUT_RULE
                ]
            }
    };

    useEffect(() => {
        if (!firstCurrency || !secondCurrency) {
            return;
        }

        computeAvailableCurrencyList(currencies);
    }, [computeAvailableCurrencyList, currencies, firstCurrency, secondCurrency]);

    return (
        <Form
            ref={formRef}
            name="Exchange-form"
            layout="vertical"
            onFinish={onSubmit}
            initialValues={{ fromAmount: firstAmount, toAmount: secondAmount }}>
            {firstCurrency &&
                <Form.Item name="fromAmount"
                    rules={computeExchangeInputRules(firstCurrency, actionType).fromExchangeRules}>
                    <ExchangeInput
                        onCurrencyChange={onFirstCurrencyChange}
                        onAmountChange={onFromAmountChange}
                        amount={firstAmount}
                        availableCurrencies={availableCurrencies}
                        defaultCurrency={firstCurrency}
                        accountAmount={getAccountAmount(firstCurrency)} />
                </Form.Item>
            }
            <div className="Exchange-body-action-type">
                <ExchangeActionType
                    action={actionType}
                    onActionChange={onActionTypeChange}
                />
            </div>
            {secondCurrency &&
                <Form.Item name="toAmount"
                    rules={computeExchangeInputRules(secondCurrency, actionType).toExchangeRules}>
                    <ExchangeInput
                        onCurrencyChange={onSecondCurrencyChange}
                        amount={secondAmount}
                        onAmountChange={onToAmountChange}
                        availableCurrencies={availableCurrencies}
                        defaultCurrency={secondCurrency}
                        accountAmount={getAccountAmount(secondCurrency)} />
                </Form.Item>
            }

            <ExchangeButton
                actionType={actionType}
                fromCurrency={firstCurrency}
                toCurrency={secondCurrency}
                buttonType={'submit'} />
        </Form>
    )
}