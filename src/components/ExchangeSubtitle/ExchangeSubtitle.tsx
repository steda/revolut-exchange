import React, { FunctionComponent } from 'react';
import { AvailableCurrencyType } from '../../models';
import './ExchangeSubtitle.scss';

interface Props {
    fromValue: number;
    fromCurrency: AvailableCurrencyType;
    toValue: number;
    toCurrency?: AvailableCurrencyType;
}

export const ExchangeSubtitle: FunctionComponent<Props> = ({
    fromValue,
    fromCurrency,
    toValue,
    toCurrency
}) => {
    return (
        <div className="ExchangeSubtitle">
            Market order - {fromValue} {fromCurrency} = {toValue} {toCurrency}
        </div>
    )
}