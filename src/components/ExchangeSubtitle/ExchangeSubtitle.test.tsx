import { render, RenderOptions } from '@testing-library/react';
import React from 'react';
import { AvailableCurrencyType } from '../../models';
import { AVAILABLE_CURRENCIES } from '../../data/AvailableCurrencies.const';
import { ExchangeSubtitle } from './ExchangeSubtitle';

test("<ExchangeSubtitle /> should match snapshot", () => {
    const component = render(
        <ExchangeSubtitle
            fromValue={1}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toValue={1.22}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />
    );
    expect(component.container).toMatchSnapshot();
});

test('<ExchangeSubtitle /> should render without crashing', () => {
    render(
        <ExchangeSubtitle
            fromValue={1}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toValue={1.22}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeSubtitle /> should render correct values', () => {
    const assertText = 'Market order - 1 EUR = 10 USD';

    const { container } = render(
        <ExchangeSubtitle
            fromValue={1}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toValue={10}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);

    expect(container.firstChild?.textContent).toEqual(assertText);
});

test('<ExchangeSubtitle /> should have correct CSS classes', () => {
    const SUBTITLE_CLASS = 'ExchangeSubtitle';
    const { container } = render(
        <ExchangeSubtitle
            fromValue={1}
            fromCurrency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
            toValue={10}
            toCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);

    expect(container.firstChild).toHaveClass(SUBTITLE_CLASS);
});