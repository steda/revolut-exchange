import { render, RenderOptions } from '@testing-library/react';
import React from 'react';
import { AvailableCurrencyType } from '../../models';
import { AVAILABLE_CURRENCIES } from '../../data/AvailableCurrencies.const';
import { ExchangeInputBalance } from './ExchangeInputBalance';

test("<ExchangeInputBalance /> should match snapshot", () => {
    const component = render(
        <ExchangeInputBalance
            amount={100}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />
    );
    expect(component.container).toMatchSnapshot();
});

test('<ExchangeInputBalance /> should render without crashing', () => {
    render(
        <ExchangeInputBalance
            amount={0}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeInputBalance /> should render correct values', () => {
    const assertText = 'Balance: 100 EUR';

    const { container } = render(
        <ExchangeInputBalance
            amount={100}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />);

    expect(container.firstChild?.textContent).toEqual(assertText);
});

test('<ExchangeInputBalance /> should have correct CSS classes', () => {
    const SUBTITLE_CLASS = 'ExchangeInputBalance';
    const { container } = render(
        <ExchangeInputBalance
            amount={100}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />);

    expect(container.firstChild).toHaveClass(SUBTITLE_CLASS);
});