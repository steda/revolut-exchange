import React, { FunctionComponent } from 'react';
import { AvailableCurrencyType } from '../../models';
import './ExchangeInputBalance.scss';

interface Props {
    amount: number;
    currency: AvailableCurrencyType;
}

export const ExchangeInputBalance: FunctionComponent<Props> = ({ amount, currency }) => (
    <div className="ExchangeInputBalance">
        Balance: {amount} {currency}
    </div>
)