import { screen, render, RenderOptions } from '@testing-library/react';
import React from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import { ExchangeTitle } from './ExchangeTitle';
import { AVAILABLE_CURRENCIES } from '../../data/AvailableCurrencies.const';

test("<ExchangeTitle /> should match snapshot", () => {
    const component = render(
        <ExchangeTitle
            exchangeActionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />
    );
    expect(component.container).toMatchSnapshot();
});

test('<ExchangeTitle /> should render without crashing', () => {
    render(
        <ExchangeTitle
            exchangeActionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeTitle /> should render correct values', () => {
    const { container } = render(
        <ExchangeTitle
            exchangeActionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />);

    const actionContent = screen.getByText(EXCHANGE_ACTION_TYPE_ENUM.SELL);
    const titleContent = screen.getByText(AVAILABLE_CURRENCIES.EUR.code);

    expect(container).toContainElement(actionContent);
    expect(container).toContainElement(titleContent);
});

test('<ExchangeTitle /> should have correct CSS classes', () => {
    const CONTAINER_CLASS = 'ExchangeTitle';
    const SPAN_CLASS = 'ExchangeTitle-action';

    const { container } = render(
        <ExchangeTitle
            exchangeActionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            currency={AVAILABLE_CURRENCIES.EUR.code as AvailableCurrencyType}
        />);

    expect(container.firstChild).toHaveClass(CONTAINER_CLASS);
    expect(container.firstChild?.firstChild).toHaveClass(SPAN_CLASS);
});