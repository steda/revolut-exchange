import React, { FunctionComponent } from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import './ExchangeTitle.scss'

interface Props {
    exchangeActionType: EXCHANGE_ACTION_TYPE_ENUM;
    currency: AvailableCurrencyType;
}

export const ExchangeTitle: FunctionComponent<Props> = ({
    exchangeActionType,
    currency
}) => {
    return (
        <div
            className="ExchangeTitle"
            data-testid="ExchangeTitle">
            <span className="ExchangeTitle-action">{exchangeActionType}</span>
            &nbsp;
            {currency}
        </div>
    )
}