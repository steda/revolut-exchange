import React, { FunctionComponent } from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import { ExchangeSubtitle } from '../ExchangeSubtitle/ExchangeSubtitle';
import { ExchangeTitle } from '../ExchangeTitle/ExchangeTitle';

interface Props {
    actionType: EXCHANGE_ACTION_TYPE_ENUM;
    convertionRate: number;
    firstCurrency: AvailableCurrencyType;
    secondCurrency: AvailableCurrencyType;
}

export const ExchangeHeader: FunctionComponent<Props> = ({
    actionType,
    convertionRate,
    firstCurrency,
    secondCurrency
}) => (
    <>
        <ExchangeTitle
            exchangeActionType={actionType}
            currency={firstCurrency}
        />
        <ExchangeSubtitle
            fromValue={1}
            fromCurrency={firstCurrency}
            toValue={convertionRate}
            toCurrency={secondCurrency}
        />
    </>
);