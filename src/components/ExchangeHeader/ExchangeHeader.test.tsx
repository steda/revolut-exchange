import { render, RenderOptions } from '@testing-library/react';
import React from 'react';
import { AvailableCurrencyType, EXCHANGE_ACTION_TYPE_ENUM } from '../../models';
import { AVAILABLE_CURRENCIES } from '../../data/AvailableCurrencies.const';
import { ExchangeHeader } from './ExchangeHeader';
import { ExchangeTitle } from '../ExchangeTitle/ExchangeTitle';

test("<ExchangeHeader /> should match snapshot", () => {
    const { container } = render(
        <ExchangeHeader
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            convertionRate={1.22}
            firstCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
            secondCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />
    );
    expect(container).toMatchSnapshot();
});

test('<ExchangeHeader /> should render without crashing', () => {
    render(
        <ExchangeHeader
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            convertionRate={1.22}
            firstCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
            secondCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />,
        document.createElement('div') as RenderOptions
    );
});

test('<ExchangeHeader /> should display only title and subtitle', () => {
    const { container, getAllByTestId } = render(
        <ExchangeHeader
            actionType={EXCHANGE_ACTION_TYPE_ENUM.SELL}
            convertionRate={1.22}
            firstCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
            secondCurrency={AVAILABLE_CURRENCIES.USD.code as AvailableCurrencyType}
        />);



    expect(getAllByTestId('ExchangeTitle')).toBeTruthy();
    expect(getAllByTestId('ExchangeTitle')).toBeTruthy();
    expect(container.childNodes).toHaveLength(2);
});